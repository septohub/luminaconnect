function onProgress() {
    const notification = document.getElementById('notification');
    notification.textContent = 'THANKS FOR THE VISIT';
    notification.classList.add('show');

    // Hide the notification after 3 seconds
    setTimeout(() => {
        notification.classList.remove('show');
    }, 3000);
}